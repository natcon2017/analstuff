# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
Sending analytic data to 3 groups of NATCON people

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

There are four groups that will be sent to:

*Group A: BGW, NACs, Nationals and NATCON staff
*Group B: NATCON IM
*Group C: NATCON PA

Required analytics:
*1. Paid versus total attendees: A 
*2. Paid in an Area/Region: A
*3. Paid in a chapter/squadron: A
*4. Heatmap based on form 1 (more info coming): A
*5. T-Shirts by size: C
*6. Extended stay by type: B
*7. DME: B
*8. All registrants gender/college/organization: B




### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


### References ###
* [mysql-connector-python - Creating Tables](https://dev.mysql.com/doc/connector-python/en/connector-python-example-ddl.html)
* [mysql-connector-python - Inserting Data](https://dev.mysql.com/doc/connector-python/en/connector-python-example-cursor-transaction.html)
* [mysql-connector-python - Updating Data](https://dev.mysql.com/doc/connector-python/en/connector-python-example-cursor-select.html)
* [Wild Apricot API - API Auth](https://help.wildapricot.com/display/DOC/Authenticating+API+access+from+a+3rd-party+server+or+application)
* [Wild Apricot API - EventRegistrations](https://help.wildapricot.com/display/DOC/EventRegistrations+API+V2+call)
