import mysql.connector

DB_USER = ''
DB_PASS = ''
DB_HOST = ''
DB_BASE = ''

cnx = mysql.connector.connect(user=DB_USER, password=DB_PASS,
                              host=DB_HOST, database=DB_BASE)

cnx.close()
